ID3 1.1.2
=========

id3 is a music stream tagging program that follows the
ID3 specification. It lets you store the following info in a tag:

* 30 character Artist name
* 30 character Album name
* 30 character Title name
*  4 character Year
* 30 character comment
*   enumerated Genre

id3 uses command-line options to specify this data:

 -t	Title
 -a	Artist
 -A	Album
 -y	Year
 -c	Comment
 -g	Genre

Anything that is not a command-line option is taken as a filename. Thus, you
can tag an entire directory with Artist, Album, Year, and Genre information
like so:

id3 -a "Crash Test Dummies" -A "God Shuffled His Feet" -y 1993 -g Pop *

If an entry contains no whitespace, you won't need to quote it - that's only
so your shell gets things right.

Unused data in fields are padded with spaces.

If you wish to use id3 as part of a system of scripts you may wish to use
the -R option which causes id3 to print out all changes and listings in an
rfc822-style format (like mail headers).

id3 -L will print out a list of all genres.

The test suite may be run both at build time (by running `make test`) and
afterwards (by running `prove t` with the TEST_ID3 environment variable set to
the full path of the id3 executable file to be tested). The `prove` tool from
the Perl distribution is used to run the tests; they require the following
Perl modules:
- File::Temp
- Path::Tiny
- Test::Command
- Test::More

An additional test also requires the `MP3::Info` module and the meow.mp3 file
from the `Video::Info` module's distribution; if `MP3::Info` is not found,
this test will be skipped. If it is run, the path to the meow.mp3 or
meow.mp3.gz example file may be specified in the TEST_ID3_MEOW environment
variable.

id3 was originally developed by Robert Woodcock <rcw@debian.org>.
In 2016, development was taken over by Peter Pentchev <roam@ringlet.net>
in a Git repository at https://gitlab.com/ppentchev/id3 and
a website at https://devel.ringlet.net/audio/id3/
